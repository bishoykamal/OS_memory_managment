#include "process.h"
#include "roundRobin.h"
#include "buddySystem.h"
#include "fileHandle.h"
#include "memoryElement.h"

//c++ convention -> file_handle : kolo small we fi underscores
//java convention -> fileHandle : camelCase

int main(){
	fileHandle * file = fileHandle::getFileHandle();
	roundRobin * roundrobin = roundRobin::getRoundRobin();
	buddySystem buddysystem;
	
	file->readFile("input.txt");
	
	////first one - ana asef
	process * p = roundrobin->schedule();
	if (p == NULL) roundrobin->_GLOBALTIME++;
	buddysystem.allocate(p);
	roundrobin->switch_runQuantum(p);


	//we want to take the new processes first and then push back the running process ...check the pdf and trace T1 
	while (roundrobin->haveProcess()){
		p = roundrobin->deSchedule(buddysystem);
		p = roundrobin->schedule();
		if (p == NULL) roundrobin->_GLOBALTIME++;
		buddysystem.allocate(p);
		roundrobin->switch_runQuantum(p);
		std::string s = file->log_txt;
		for (int u = 0; u < s.size();u++)
			std::cout << s[u];
		std::cout << std::endl << std::endl;
	}

	file->writeFile();
	return 0;
}

