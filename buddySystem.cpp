#include "buddySystem.h"
#include "process.h"
#include "memoryElement.h"

buddySystem::buddySystem()
{
	root = new memoryElement();
	root->size = 1024;
	root->parent = NULL;
	root->left = NULL;
	root->right = NULL;
	root->start = 0;
	root->end = 1023;
	root->isAllocated = false;
}

memoryElement * buddySystem::split(memoryElement *p, int size){
	if (size <= 1 || p->size<size)return NULL;
	if (p->size == size)return p;

	memoryElement *left = new memoryElement(p,true);
	memoryElement *right = new memoryElement(p,false);
	p->left = left;
	p->right = right;
	p->isAllocated = true;
	split(left,size);
}

int buddySystem::findPowerOfTwo(int size){
	return pow(2, ceil(log2(size)));
}
memoryElement * buddySystem::traverse(){
	memoryElement *min = root;
	std::queue<memoryElement *> bfs;
	bfs.push(root);
	while (!bfs.empty()){
		memoryElement *p = bfs.front();
		bfs.pop();
		if (p->left != NULL)bfs.push(p->left);
		if (p->right != NULL)bfs.push(p->right);
		if (p->size < min->size&&p->isAllocated==false)min = p;
	}
	return min;
}
void buddySystem::allocate(process *pr){

	if (pr == NULL) return;
	if (pr->memAllocated)return;

	int size = findPowerOfTwo(pr->memSize);

	memoryElement *outTraverse = traverse();
	if (outTraverse == root&&!(root->left==NULL&&root->right==NULL)){
		pr->incrementIgnores();
		return;
	}

	memoryElement *toBeAlloc = split(outTraverse, size);

	toBeAlloc->parent->isAllocated = true;
	toBeAlloc->isAllocated = true;
	pr->startMemLoc = toBeAlloc->start;
	pr->endMemLoc = toBeAlloc->end;
	pr->memAllocated = true;
	pr->allocatedMemory = toBeAlloc;
}


void buddySystem::merge(memoryElement *p){
	if (p->parent == NULL)return;
	memoryElement *parent = p->parent;
	if (parent->left->isAllocated == false && parent->right->isAllocated == false){
		delete parent->left;
		delete parent->right;
		parent->left = NULL;
		parent->right = NULL;
		parent->isAllocated = false;
		merge(parent);
	}
}

void buddySystem::deallocate(process *pr){

	memoryElement *toBeDeAlloc = pr->allocatedMemory;

	toBeDeAlloc->isAllocated = false;
	merge(toBeDeAlloc);

}




/*
bool buddySystem::memAllocate(int memsize, int & _s, int & _e){
	return false;
}

void buddySystem::memdeAllocate(int start, int end){

}
*/
void buddySystem::memProcess(process * p){

}

void buddySystem::dememProcess(process * p){

}

buddySystem::~buddySystem()
{
}
