#pragma once
#include "includes.h"
class process;
//SINGLETON CLASS - initialized by getFileHandle()

class fileHandle
{

	/*
	*	fileHandle Class:
	*	- Singleton class
	*	- use the static function getFileHandle() to return a pointer for the class
	*/
	static fileHandle * instance;
	fileHandle();

	std::string output_txt = "";
	
	std::vector <process *> terminatedProcesses;

public:
	std::string log_txt = "";
	// used to get the roundRobin singleton
	static fileHandle * getFileHandle();

	/*
	* name: read file
	* params: filename
	* gets the roundrobin instance using {roundRobin * roundrobin = roundRobin::getRoundRobin()}
 	* reads first line : Quantum
	* reads second line : switching time
	* sets quantum and switching time using @roundrobin->set_QUANTUM_SWITCHINGTIME(int q, int st)
	* loops untill EOF
	*	- reads process data
	*	- construct a new process with its data
	*	- use @roundrobin->addProcess(process *) to add it
	*/
	void readFile(std::string filename);

	/*
	* name: write file
	* writes both the strings to files : @output_txt , @log_txt
	* OR: if we need to print the processes in output.txt in ascending order like
	* the pdf, sort the processes in @processes and use them to write the file _ all data is set 
	*/
	void writeFile();
	
	/*
	* name: add finished process
	* adds the process pointer * to the @processes vector 
	* {adds to string or writes to file} the data of the process , it's all set before , no need to calculate anything
	*/
	void addFinishedProcess(process *);

	/*
	* name: log data
	* params: the queue of current processes, and the process to log data about
	* adds to the string: [UPDATE: we can remove the strings and make this function and @addFinishedProcess print to the files]
		- "Queue" : all the processes names in the vector
		- if process.memAllocated
			executing process started at {currentTime-QUANTUM-(-ve part of p.remainingTime)}, stopped at {currentTime}, remaining {p.remaningTime > 0} , p.memStart , p.memEnd
			{if remaining time < 0 -> "finished at" instead of "stopped at"}
	*/
	void log(std::queue <process *> v, process*, int CurrentTime);

	/*
	* name: switching 
	* params: int time starting switching, int time ending switching
	* prints/adds to string the "process switching" line 
	*/
	void switching(int before, int after);

	~fileHandle();
};

