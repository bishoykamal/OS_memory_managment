#pragma once
#include "includes.h"

class memoryElement
{
	
		

public:
	int size;
	int start;
	int end;
	memoryElement * parent;
	memoryElement * left;
	memoryElement * right;
	bool isAllocated;

	memoryElement(memoryElement* p, bool isLeft);
	memoryElement();
	~memoryElement();
};

