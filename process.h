#pragma once
#include "includes.h"
class roundRobin;
class memoryElement;

class process 
{
	
public:

	//set by reading Class
	std::string id;
	int arrivalTime;
	int runTime;
	int memSize;

	//
	int startTime = -1;
	int remainingTime; //calculated by RoundRobin _ initialized as runTime
	bool memAllocated = false;
	int startMemLoc = -1; //initialized to -1
	int endMemLoc = -1;
	int IgnoresCount = 0; //initialez to 0
	int finishTime = -1;

	memoryElement * allocatedMemory;

	/*
	* name: process Initializer
	* params: int processID, int processArrivalTime, int processRunTime, int processMemorySize
	* initialize a new process and assign itself (process *) to the RoundRobin vector containing all processes
	* by calling roundRobin::addProcess(this) -> use roundRobin * roundrobin = roundRobin::getRoundRobin() to get
	* roundRobin instance
	* calledBy: reading Class
	*/
	process(std::string, int, int, int); 
	
	/*
	* name: run
	* params: time quantum to run
	* decrease the @remainingTime of the process by the quantum value if (@memAllocated) 
	* and returns the -ve part (if exist) of the @remainingTime
	* calledBy roundRobin::switch_runQuantum
	*/
	int run(int quantum);
	
	/*
	* name: set process memory location (! THIS FUNCTION DON'T ALLOCATE MEMORY.)
	* params: int startMemoryLocation, int endMemoryLocation
	* set the process @startMemLoc and @endMemLoc to start, end and set @memAllocated to true
	* calledBy buddySystem::memProcess
	*/
	void setMemLocation(int start, int end);
	
	/*
	* name: increment process ignores counter
	* params: none
	* increments the @IgnoresCount of the process
	* calledBy buuddySystem::memProcess
	*/
	void incrementIgnores();
	
	
	~process();
};

