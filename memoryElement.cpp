#include "memoryElement.h"


memoryElement::memoryElement(memoryElement* p, bool isLeft){
	this->size = p->size / 2;
	this->left = NULL;
	this->right = NULL;
	this->isAllocated = false;
	this->parent = p;
	if (isLeft){
		this->start = p->start;
		this->end = (p->end+p->start) / 2;
	}
	else{
		this->start = ((p->end+p->start) / 2) + 1;
		this->end = p->end;
	}

}

memoryElement::memoryElement(){
}



memoryElement::~memoryElement()
{
}
