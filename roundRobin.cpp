#include "roundRobin.h"
#include "process.h"
#include "fileHandle.h"
#include "buddySystem.h"

roundRobin * roundRobin::instance = nullptr; //initializing the static pointer to null (required by system)
// (roundRobin *) : it's type
// (roundRobin::) : class as it is static
// (instance)	   : it's name

roundRobin * roundRobin::getRoundRobin(){
	if (!instance){
		instance = new roundRobin;
	}
	//printf("returning roundRobin ..\n");
	return instance;
}

void roundRobin::addProcess(process * p){
	this->allProcesses.push_back(p);
}

void roundRobin::set_QUANTUM_SWITCHINGTIME(int q, int st){
	this->_QUANTUM = q;
	this->_SWITCHINGTIME = st;
}

process * roundRobin::deSchedule(buddySystem &b){
	if (!processQueue.size())
		return NULL;

	fileHandle * file = fileHandle::getFileHandle();
	process * p = processQueue.front();
	processQueue.pop();
	if (p->remainingTime > 0 && p->IgnoresCount < 5){
		processQueue.push(p);
		file->log(this->processQueue, p, _GLOBALTIME);
	} else {
		p->finishTime = _GLOBALTIME;
		file->log(this->processQueue, p, _GLOBALTIME);
		file->addFinishedProcess(p);
		b.deallocate(p);//deallocation if the process ends by bishoy and mina
		// delete p; -> delete ba3d ma n3ml dememProcess hnak y3ni 
	}
	return p;
}

process * roundRobin::schedule(){
	bool switcher = true;
	if (processQueue.size() == 0) switcher = false;


	for (int i = 0; i < (int)allProcesses.size(); i++){
		if (allProcesses[i]->arrivalTime <= _GLOBALTIME){
			process * p = allProcesses[i];
			allProcesses.erase(allProcesses.begin() + i);
			i--;
			processQueue.push(p);
		}
	}

	if (switcher){
		fileHandle * file = fileHandle::getFileHandle();
		file->switching(_GLOBALTIME, _GLOBALTIME + _SWITCHINGTIME);
		_GLOBALTIME += _SWITCHINGTIME;
	}
	if (processQueue.empty()) return NULL;
	return processQueue.front();
	 
}

void roundRobin::switch_runQuantum(process * p){
	if (p == NULL) return;
	if (p->memAllocated){
		if (p->startTime == -1)
			p->startTime = _GLOBALTIME;
		int x = p->run(_QUANTUM);//by mina and bishoy for tracing only
		_GLOBALTIME += x;
	}
}

bool roundRobin::haveProcess(){
	return (allProcesses.size() || processQueue.size());
}

roundRobin::roundRobin()
{
	printf("roundRobin created ..\n");
}


roundRobin::~roundRobin()
{
}
