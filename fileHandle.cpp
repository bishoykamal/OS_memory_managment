#include "fileHandle.h"
#include "process.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "roundRobin.h"

fileHandle * fileHandle::instance = nullptr; //initializing the static pointer to null (required by system)
// (fileHandle *) : it's type
// (fileHandle::) : class as it is static
// (instance)	   : it's name

fileHandle * fileHandle::getFileHandle(){
	if (!instance){
		instance = new fileHandle;
	}
	//printf("returning roundRobin ..\n");
	return instance;
}

void fileHandle::readFile(std::string name){
	
	roundRobin * roundrobin = roundRobin::getRoundRobin();

	std::string s;
	std::string line;
	std::ifstream input;
	std::string dumb;

	input.open(name);

	if (input.is_open())
	{

		//Quantum	#n
		int Quantum;
		input >> dumb;
		input >> Quantum;

		//Switch	#n
		int Switch;
		input >> dumb;
		input >> Switch;

		//set the global values
		roundrobin->set_QUANTUM_SWITCHINGTIME(Quantum, Switch);

		//process_id run_time arrival_time mem_size
		input >> dumb; input >> dumb; input >> dumb; input >> dumb;

		//process data in a loop
		std::string id; int run_time; int arrival_time; int mem_size;

		while (!input.eof())
		{
			input >> id;
			input >> run_time;
			input >> arrival_time;
			input >> mem_size;
			process * p = new process(id, run_time, arrival_time, mem_size);
			roundrobin->addProcess(p);
		}
		input.close();
	}
	else std::cout << " Unable to open file.";
}

fileHandle::fileHandle()
{
	printf("fileHandle created ..\n");
}

void fileHandle::writeFile(){
	
	output_txt = "process_id\trun_time\tarrival_time\tfinish_time\tmem_size\tmem_start mem_end\n" + output_txt;

	std::ofstream out;
	out.open("output.txt");
	out << output_txt;
	out.close();

	std::ofstream logger;
	logger.open("log.txt");
	logger << log_txt;
	logger.close();
}

void fileHandle::addFinishedProcess(process * p){
	terminatedProcesses.push_back(p);
	output_txt = output_txt + p->id + "\t" + std::to_string(p->runTime) + "\t" + std::to_string(p->arrivalTime) + "\t" + std::to_string(p->finishTime)
		+ "\t" + std::to_string(p->memSize) + "\t" + std::to_string(p->startMemLoc) + "\t" + std::to_string(p->endMemLoc) + "\n";
}

void fileHandle::log(std::queue <process *> v, process* p, int CurrentTime){
	
	roundRobin * roundrobin = roundRobin::getRoundRobin();
	
	if (!v.empty()){
		log_txt = log_txt + "Queue: ";
		while (!v.empty()){
			process * p = v.front();
			v.pop();
			log_txt = log_txt + p->id;
		}
		log_txt = log_txt + "\n";
	}
	
	//why the starting time is quantum-remainig or the quantunm ?? it must be the global time ....check the assignment pdf by mina and bishoy
	if (p->memAllocated && p->remainingTime > 0){
		log_txt = log_txt + "Executing process " + p->id + "\t :" + " started at " + std::to_string(CurrentTime - roundrobin->_QUANTUM) + ",stopped at " + std::to_string(CurrentTime) + ", remaining " + std::to_string(p->remainingTime)
			+ ", memory starts at " + std::to_string(p->startMemLoc) + ", memory ends at " + std::to_string(p->endMemLoc) + "\n";
	} else if (p->memAllocated && p->remainingTime <= 0){
		log_txt = log_txt + "Executing process " + p->id + "\t :" + " started at " + std::to_string(CurrentTime - roundrobin->_QUANTUM - p->remainingTime) + ",finished at " + std::to_string(CurrentTime) + ", remaining " + std::to_string(p->remainingTime)
			+ ", memory starts at " + std::to_string(p->startMemLoc) + ", memory ends at " + std::to_string(p->endMemLoc) + "\n";
		
	} else if (!p->memAllocated){
		log_txt = log_txt + "Executing process " + p->id + " Sorry!there wasn't enough space.\n";
	}

}

void fileHandle::switching(int before, int after){
	log_txt = log_txt + "Process switching, started at " + std::to_string(before) + ", finished at " + std::to_string(after) + "\n";
}


fileHandle::~fileHandle()
{
}
