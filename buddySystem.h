#pragma once
#include "includes.h"
class process;
class memoryElement;



class buddySystem
{
	
	memoryElement *root;

	memoryElement * split(memoryElement *p,int size);

	void merge(memoryElement *p);


	//deallocate set isallocated false
	int findPowerOfTwo(int size);

	
	memoryElement * traverse();

	public:
		void allocate(process *pr);
		void deallocate(process *pr);



	//TODO: memory abstraction variable here

	/*
	* name: allocate memory
	* params: int memorySize, int start (by reference) , int end (by reference)
	* tries to allocate memory in the memory
	* returns true if it could allocate memory and changes the _s, _e to the start and end
	* locations of the allocated memory
	* returns false it it could not allocate memory
	*/
	//bool memAllocate(int memsize, int & _s, int & _e);
	
	/*
	* name: deallocate memory
	* params: int start , int end
	* deallocates the segment [start-end] in the memory
	*/
	//void memdeAllocate(int start, int end);


	buddySystem();
	/*
	* name: mem Process
	* params: process pointer p
	* tries to allocate new memory to the process using @memAllocate(int memsize, int & _start, int & _end) if 
	* process.memAllocated was false , if it succeeded it sets the p.memAllocated to true
	* and sets the memStart and memEnd of p to local _start and local _end using p.setmemLoc(_start, _end)
	* if it fails to allocate memory, it increments the igonreCounter of p
	** Assume an overhead of 6B on each process (i.e. if a process size is 50B, then it will occupy 56B on the memory).
	* calledBy: main()
	*/
	void memProcess(process * p);

	/*
	* name: demem Process
	* params: process pointer p
	* if p.memAllocated was true and finishTime not equal -1
	* deallocates memory [p.memStart - p.memEnd] using @memdeAllocate(int start, int end) 
	* else return 
	* calledBy: main()
	*/
	void dememProcess(process * p);

	~buddySystem();
};

