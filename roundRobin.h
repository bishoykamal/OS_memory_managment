#pragma once
#include "includes.h"
class process;
class buddySystem;

//SINGLETON CLASS - initialized by getRoundRobin()

class roundRobin
{

	/*
	*	roundRobin Class:
	*	- Singleton class
	*	- use the static function getRoundRobin() to return a pointer for the class
	*/
	static roundRobin * instance;
	roundRobin();

	std::vector <process *> allProcesses;
	std::queue <process *> processQueue;

	
public:
	int _QUANTUM;
	int _SWITCHINGTIME;
	int _GLOBALTIME;


	// used to get the roundRobin singleton
	static roundRobin * getRoundRobin();

	/*
	* name: add process
	* params: process * p
	* adds a new process pointer to the @allProcesses queue  
	* calledBy: reading Class
	*/
	void addProcess( process * p);

	/*
	* name: set quantum & and switching time
	* params: int quantum, int switching time
	* sets the @_QUANTUM and @_SWITCHINGTIME
	* calledBy: reading Class
	*/
	void set_QUANTUM_SWITCHINGTIME(int q,int st);

	/*
	* name: de-schedule
	* pops the first process (as it's the only one that may have run and finished) in the @processQueue 
	* if remaining time > 0 and ignoreCounter < 5 push it back to the queue and call fileHandle->log(processQueue,firstProc,_GLOBALTIME)
	* else : sets finishing time then adds it to file data using fileHandle->addFinishedProcess(process *) and call fileHandle->log.....
	* if process.ignores == 5 -> error and remove
	* returns pointer to it
	* calledBy: main()
	*/
	process * deSchedule(buddySystem &b);

	/*
	* name: schedule
	* adds new processes from @allProcesses to  @processQueue according to arrival time
	* if number of processes left in @processQueue >= 2
	*	adds the @_SWITCHINGTIME to the @_GLOBALTIME
	*	calls file->switch(int before, int after) to print the switching line
	*	returns the first process * in the queue [DON'T POP IT FROM THE QUEUE] 
	* calledBy: main()
	*/
	process * schedule();


	/*
	* name: switch & run Quantum
	* params: process pointer (that needs to be run)
	* if process.memAllocated was true -> GLOBAL_TIME += QUANTUM + process.run(QUANTUM) {switching time is added in @schedule()}
	* calledBy: main()
	*/
	void switch_runQuantum( process * );

	/*
	* name: have processes
	* returns true if @allProcesses.size or @processQueue.size
	* calledBy: main()
	*/
	bool haveProcess();

	~roundRobin();
};

