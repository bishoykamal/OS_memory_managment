#include "process.h"
#include "roundRobin.h"
#include "memoryElement.h"

process::process(std::string id, int run_time, int arrival_time, int mem_size){
	this->id = id;
	this->runTime = run_time;
	this->arrivalTime = arrival_time;
	this->memSize = mem_size+6;
	this->allocatedMemory = NULL;
	this->remainingTime = run_time;
}

/*
this function was modified completely by bishoy and mina as it was calculating wrong remaining time
the remaining time was -20 and global time was -20 :D
*/
int process::run(int quantum){
	if (this->memAllocated){
		if (this->remainingTime > quantum)
		{
			this->remainingTime -= quantum;
			return quantum;
		}
		else{
			int temp = this->remainingTime;
			this->remainingTime = 0;
			return temp;
		}
	}
	else{
		return 0;
	}
}

void process::setMemLocation(int start, int end){
	this->startMemLoc = start;
	this->endMemLoc = end;
}

void process::incrementIgnores(){
	this->IgnoresCount++;
}

process::~process()
{
}
